<?php


namespace app\home\controller;


use think\Db;

class Confirm extends Base
{
    public function stu_info_confirm(){
        $id = input('id');
        $stu_info = Db::name('students')->where('id',$id)->find();
        $this->assign('id',$id);
        $this->assign('data',$stu_info);
        return $this->fetch();
    }

    public function test_confirm(){
        $test_no = input('test_no');
        $test_info = Db::name('test_info a')
            ->join('test_stu b','a.id=b.test_id','left')
            ->where('b.test_no',$test_no)
            ->field('a.*')
            ->find();
        $this->assign('data',$test_info);
        return $this->fetch();
    }

    public function test_notice(){
        $id = input('id');
        $notice = Db::name('test_info')->where('id',$id)->value('remark');
        $this->assign('data',$notice);
        return $this->fetch();
    }

}