<?php


namespace app\admin\controller;


use think\Db;
use app\admin\model\MemberList;


class Students extends Base{
    public function students_list(){
        
		$key=input('key');
		if($key == null){
			$data = Db::name('students')->where(true)->paginate(config('paginate.list_rows'),false,['query' => get_query()]);
			
		}else{
			$data = Db::name('students')->where('name',$key)->paginate(config('paginate.list_rows'),false,['query' => get_query()]);
		} 
        $page = $data->render();
        $data = $data->all();
        $page = preg_replace("(<a[^>]*page[=|/](\d+).+?>(.+?)<\/a>)", "<a href='javascript:ajax_page($1);'>$2</a>", $page);
        $this->assign('page', $page);
        $this->assign('data', $data);
        if (request()->isAjax()){
            return $this->fetch('ajax_students_list');

        }else{
            return $this->fetch('students_list');
        }

    }
	
	
		/*
     * 添加用户显示
     */
	public function students_add(){
		/*$province = Db::name('students')->where ( array('id'=>1) )->select ();
		$this->assign('province',$province);
		$member_group=Db::name('member_group')->order('member_group_order')->select();
		$this->assign('member_group',$member_group);*/
		return $this->fetch();
	}

	/*
     * 添加用户操作
     */
	public function students_runadd(){
		
		$name=input('name');
		$student_no=input('student_no');
		$class_id=input('class_id');
		$test_no=input('test_no');
		$create_time=time();
		
		$sql="insert into lyc_students(student_no,name,class_id,test_no,create_time) values(?,?,?,?,?)";
		//$sql="update lyc_students set student_no=?,name=?,class_id=?,test_no=?,create_time=?";
		//Db::execute("update think_user set name=:name where status=:status", ['name' => 'thinkphp', 'status' => 1]);
		$a=Db::execute($sql,[$student_no,$name,$class_id,$test_no,$create_time]);
		if($a !== false){
			$this->success('学生添加成功',url('admin/Students/students_list'));
		}else{
			$this->error('学生添加失败',url('admin/Students/students_list'));
		}
		
	}
	


/*
     * 修改用户信息界面
     */
	public function students_edit(){
		$sql="select * from lyc_students where id=?";
		$students_list_edit=Db::query($sql,[input('id')]);
		//$students_list_edit=Db::name('lyc_students')->where(array('id'==input('id')))->select();
		//dump($students_list_edit[0]["id"]);
		$this->assign('students_list_edit',$students_list_edit[0]);
		return $this->fetch();
		
	}
	/*
	 * 修改用户操作
	 */
	public function students_runedit(){
		
			//dump(input('id'));
			
			$name=input('name');
			$student_no=input('student_no');
			$class_id=input('class_id');
			$test_no=input('test_no');
			$create_time=time();
		
			$sql="update lyc_students set student_no=?,name=?,class_id=?,test_no=?,create_time=? where id=?";
			$a=Db::execute($sql,[$student_no,$name,$class_id,$test_no,$create_time,input('id')]);
			
			if($a!==false){
				$this->success('修改成功',url('admin/Students/students_list'));
			}else{
				$this->error('修改失败',url('admin/Students/students_list'));
			}
				
				
	}	


	/*
     * 学生删除
     */
	public function students_del()
	{
		$id=input('id');
	
		$sql ="delete from lyc_students where id=?";
		$a=Db::execute($sql,[$id]);
	
			if($a!==false){
				$this->success('学生删除成功',url('admin/Students/students_list'));
			}else{
				$this->error('学生删除失败',url('admin/Students/students_list'));
			}
		
	}
	


	
}